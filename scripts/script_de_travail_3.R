library(bsplus)
library(COGiter)
library(dplyr)
library(DT)
library(formattable)
library(ggplot2)
library(highcharter)
library(htmltools)
library(htmlwidgets)
library(jpeg)
library(kableExtra)
library(leaflet)
library(leaflet.extras)
library(lubridate)
#library(phantomjs)
#library(plotly)
library(png)
library(rAmCharts)
library(sf)
library(shinyWidgets)
library(shiny)
library(shinyBS)
library(shinycustomloader)
library(shinydashboard)
library(shinydreal)
library(tidyverse)
library(viridis)
library(webshot)


load("app/ort_effectifs_transports.RData")
load("app/fonds_de_carte_52.RData")


test <- donnees_cogifiees_communes %>%
  filter(TypeZone=="Epci") %>%
  #filter(annee==input$mon_annee) %>%
  filter(annee==2017) %>%
  group_by(Zone, CodeZone, secteur, annee) %>%
  summarise(effectif = sum(effectif, na.rm = T)) %>%
  ungroup() %>% 
  rename(EPCI = CodeZone) %>% 
  spread(key = secteur, value = effectif, fill = 0) %>%
  mutate(
    `Ensemble des secteurs` = `Activités financières et d'assurance` + `Activités immobilières` + `Activités scientifiques et techniques, services administratifs et de soutien ` + `Administrations publiques, enseignement, santé humaine et action sociale` + `Autres activités de services` + `Cokéfaction et raffinage` + `Commerce et réparation d'automobiles et de motocycles` + `Construction` + `Fabrication d'autres produits industriels` + `Fabrication d'équipements électriques, électroniques et informatiques` + `Fabrication de denrées alimentaires` + `Fabrication de matériels de transport` + `Hébergement et restauration` + `Industries extractives` + `Information et communication` + `Transports et entreposage`
  ) %>%
  mutate(ratio_1 = `Transports et entreposage` / `Ensemble des secteurs`,
         ratio_2 = paste0(round(((`Transports et entreposage` / `Ensemble des secteurs`) * 100
         ), 1), " %")) %>%
  select(Zone, EPCI, annee, ratio_1, ratio_2) %>%
  mutate(ratio_2=str_replace(ratio_2, "[.]", ","))
  
test_2 <- epci_geo_52 %>%
  select(EPCI) %>% 
  right_join(test)

# bins <- quantile(test_2$effectif, probs = c(0, 0.05, 0.4, 0.7, 0.95, 1), na.rm=TRUE)
breaks=c(0, 0.01, 0.05, 0.1, 0.15, 1)
pal <- colorBin(c("white", "#00bebe", "#00465a"), domain = test_2$ratio_1, bins = breaks)
labels <- sprintf("<strong>%s</strong><br/> Part des transports en %s : %s", test_2$Zone, test_2$annee, test_2$ratio_2) %>%
  lapply(htmltools::HTML)

leaflet(test_2) %>%
  # addProviderTiles(providers$CartoDB.Positron) %>%
  # addTiles(attribution = HTML(paste(span(style="color:#00465a","Source : Acoss (données au 31 décembre")))) %>%
  # setView(lng = -1.5533600, lat = 47.2172500, zoom = 8) %>%
  setMaxBounds(lng1 = -2.5, lat1 = 45.7, lng2 = 1.5, lat2 = 48.9) %>% 
  addControl(html = tags$div(
    HTML(paste(span(style="color:#00465a; font-size:18px",
                           "Part du secteur des transports et de l'entreposage", tags$br(),
                    "dans l’ensemble des effectifs salariés", tags$br(), "industriels et commerciaux"),
               "<b>", "<br><br>Passer la souris sur un EPCI", "</b>")), align="center"), position = "topright") %>%
  addControl(html = tags$div(
    HTML(paste(span("Source : Acoss", tags$br(), "Champ : établissements affiliés à l’Acoss (emplois agricoles et fonction publique non compris)", style="color:#00465a; font-size:10px")))), position = "bottomleft") %>%
  # addPolygons(data = epci_geo_52, weight = 2, opacity = 1, fillOpacity = 0, color = "white") %>%
  addPolygons(data = test_2, fillColor = ~pal(ratio_1), weight = 2,
              opacity = 1, fillOpacity = 1, color = "white") %>%
  addPolygons(data = departements_geo_52, weight = 2, opacity = 1, fillOpacity = 0, color = "black") %>%
  addPolygons(data = test_2, fillColor = "", color = "", fillOpacity = 0, 
              dashArray = "1",
              highlight = highlightOptions(weight = 2, dashArray = "",
                                           color = "#3c0064", opacity = 1, fillOpacity = 0,
                                           bringToFront = TRUE, sendToBack = TRUE),
              label = labels,
              labelOptions = labelOptions(style = list("font-weight" = "normal", padding = "3px 5px"),
                                          textsize = "10px", direction = "auto")) %>%
  addLegend(data = test_2, pal = pal, values = ~ratio_1, opacity = 1,
            title = paste("Part des transports<br>au 31 déc.", annee),
            position = "bottomright",
            labFormat = labelFormat(prefix = "Entre ", between = " et ", suffix = " %",
                                    transform = function(x) 100 * x),
            na.label = "Aucun salarié") %>%
  addFullscreenControl() %>%
  onRender(
    "function(el, x) {
            L.easyPrint({
              sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
              filename: 'mymap',
              exportOnly: true,
              hideControlContainer: true
            }).addTo(this);
            }"
  )
