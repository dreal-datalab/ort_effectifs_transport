library(tidyverse)
library(dplyr)

options(scipen = 999)

  

test_1 <- donnees_regions %>%
    filter(annee == params$annee) %>%
    group_by(annee, region) %>%
    summarise(effectif = sum(effectif, na.rm = T)) %>%
    spread(key = region, value = effectif, fill = 0) %>%
    mutate(
      `France métropolitaine` = `Auvergne-Rhône-Alpes` + `Bourgogne-Franche-Comté` +
        `Bretagne` + `Centre-Val de Loire` + `Corse` + `Grand Est` + `Hauts-de-France` +
        `Île-de-France` + `Normandie` + `Nouvelle-Aquitaine` + `Occitanie` + `Pays de la Loire` +
        `Provence-Alpes-Côte d'Azur`
    ) %>%
    gather(key = region, value = effectif, 2:15) %>%
    spread(key = annee, value = effectif, fill = 0)
    

    
    
    mutate(sous_secteur = ifelse(
      ape == c("49.31Z", "49.32Z", "49.39A", "49.39B"),
      "Transports routiers de voyageurs",
      "Autres"
    )) %>% 
  

    df <- data.frame("Sample" = 1:30,
                     "Individual" = c("a", "b", "c"),
                     "Repeat" = 1:3)

    
    view(donnees_transport_regions %>%
           filter(region == "Pays de la Loire") %>%
           group_by(annee, sous_secteur) %>%
           summarise(effectif = sum(effectif, na.rm = T))%>%
           spread(key = annee, value = effectif, fill = 0) %>%
           mutate(evo2004 = round(((`2004`-`2003`)/`2003`) * 100, 1)) %>%
           mutate(evo2005 = round(((`2005`-`2004`)/`2004`) * 100, 1)) %>%
           mutate(evo2006 = round(((`2006`-`2005`)/`2005`) * 100, 1)) %>%
           mutate(evo2007 = round(((`2007`-`2006`)/`2006`) * 100, 1)) %>%
           mutate(evo2008 = round(((`2008`-`2007`)/`2007`) * 100, 1)) %>%
           mutate(evo2009 = round(((`2009`-`2008`)/`2008`) * 100, 1)) %>%
           mutate(evo2010 = round(((`2010`-`2009`)/`2009`) * 100, 1)) %>%
           mutate(evo2011 = round(((`2011`-`2010`)/`2010`) * 100, 1)) %>%
           mutate(evo2012 = round(((`2012`-`2011`)/`2011`) * 100, 1)) %>%
           mutate(evo2013 = round(((`2013`-`2012`)/`2012`) * 100, 1)) %>%
           mutate(evo2014 = round(((`2014`-`2013`)/`2013`) * 100, 1)) %>%
           mutate(evo2015 = round(((`2015`-`2014`)/`2014`) * 100, 1)) %>%
           mutate(evo2016 = round(((`2016`-`2015`)/`2015`) * 100, 1)) %>%
           mutate(evo2017 = round(((`2017`-`2016`)/`2016`) * 100, 1)) %>%
           mutate(evo2018 = round(((`2018`-`2017`)/`2017`) * 100, 1)) %>%
           mutate(evo2019 = round(((`2019`-`2018`)/`2018`) * 100, 1)) %>%
           mutate(evo = round(((`2019`-`2003`)/`2003`) * 100, 1)) %>%
           mutate(tcam = round(rowMeans(select(., evo2004:evo2019)), 1)) %>%
           mutate(sous_secteur_2 = sous_secteur) %>% 
           select(-`2003`, -`2004`, -`2005`, -`2006`, -`2007`, -`2008`, -`2009`, -`2010`, -`2011`, -`2012`, -`2013`, -`2014`, -`2015`, -`2016`, -`2017`, -`2018`, -`2019`))

  view(donnees_transport_regions %>%
         filter(region == "Pays de la Loire") %>%
         group_by(annee, sous_secteur, ape) %>%
         summarise(effectif = sum(effectif, na.rm = T))%>%
         spread(key = annee, value = effectif, fill = 0) %>%
         #filter(sous_secteur=="Transports routiers de marchandises") %>% 
         mutate(evo2004 = round(((`2004`-`2003`)/`2003`) * 100, 1)) %>%
         mutate(evo2005 = round(((`2005`-`2004`)/`2004`) * 100, 1)) %>%
         mutate(evo2006 = round(((`2006`-`2005`)/`2005`) * 100, 1)) %>%
         mutate(evo2007 = round(((`2007`-`2006`)/`2006`) * 100, 1)) %>%
         mutate(evo2008 = round(((`2008`-`2007`)/`2007`) * 100, 1)) %>%
         mutate(evo2009 = round(((`2009`-`2008`)/`2008`) * 100, 1)) %>%
         mutate(evo2010 = round(((`2010`-`2009`)/`2009`) * 100, 1)) %>%
         mutate(evo2011 = round(((`2011`-`2010`)/`2010`) * 100, 1)) %>%
         mutate(evo2012 = round(((`2012`-`2011`)/`2011`) * 100, 1)) %>%
         mutate(evo2013 = round(((`2013`-`2012`)/`2012`) * 100, 1)) %>%
         mutate(evo2014 = round(((`2014`-`2013`)/`2013`) * 100, 1)) %>%
         mutate(evo2015 = round(((`2015`-`2014`)/`2014`) * 100, 1)) %>%
         mutate(evo2016 = round(((`2016`-`2015`)/`2015`) * 100, 1)) %>%
         mutate(evo2017 = round(((`2017`-`2016`)/`2016`) * 100, 1)) %>%
         mutate(evo2018 = round(((`2018`-`2017`)/`2017`) * 100, 1)) %>%
         mutate(evo2019 = round(((`2019`-`2018`)/`2018`) * 100, 1)) %>%
         mutate(evo = round(((`2019`-`2003`)/`2003`) * 100, 1)) %>%
         #mutate(tcam = round(rowMeans(select(., evo2004:evo2019)), 1)) %>%
         select(-`2003`, -`2004`, -`2005`, -`2006`, -`2007`, -`2008`, -`2009`, -`2010`, -`2011`, -`2012`, -`2013`, -`2014`, -`2015`, -`2016`, -`2017`, -`2018`, -`2019`)) 
  
  # Tableau qui permet d'avoir les effectifs communaux (au besoin, faire un tri descendant dans le title)
  view(donnees_cogifiees_transports_communes %>%
         filter(annee>2017) %>%
         #filter (TypeZone %in% c("Départements", "Régions")) %>%
         group_by(Zone, annee, sous_secteur, ape) %>%
         summarise(effectif = sum(effectif, na.rm = T)) %>%
         spread(key = annee, value = effectif, fill = 0) %>%
         filter(sous_secteur=="Entreposage, stockage et manutention") %>%
         filter(ape=="52.24B"))
      
    
view(donnees_cogifiees_transports_communes %>%
       filter (TypeZone %in% c("Départements", "Régions")) %>%
       group_by(Zone, annee, sous_secteur) %>%
       summarise(effectif = sum(effectif, na.rm = T)) %>%
       spread(key = sous_secteur, value = effectif, fill = 0) %>%
       mutate(effectif = `Activités de poste et de courrier` + `Autres activités` + `Entreposage, stockage et manutention` + `Services auxiliaires des transports` + `Transports aériens` + `Transports ferroviaires` + `Transports maritimes et fluviaux` + `Transports routiers de marchandises` + `Transports routiers de voyageurs`) %>%
       select(-`Activités de poste et de courrier`, -`Autres activités`, -`Entreposage, stockage et manutention`, -`Services auxiliaires des transports`, -`Transports aériens`, -`Transports ferroviaires`, -`Transports maritimes et fluviaux`, -`Transports routiers de marchandises`, -`Transports routiers de voyageurs`) %>%
       spread(key = annee, value = effectif, fill = 0) %>%
       mutate(evo2007 = round(((`2007`-`2006`)/`2006`) * 100, 1)) %>%
       mutate(evo2008 = round(((`2008`-`2007`)/`2007`) * 100, 1)) %>%
       mutate(evo2009 = round(((`2009`-`2008`)/`2008`) * 100, 1)) %>%
       mutate(evo2010 = round(((`2010`-`2009`)/`2009`) * 100, 1)) %>%
       mutate(evo2011 = round(((`2011`-`2010`)/`2010`) * 100, 1)) %>%
       mutate(evo2012 = round(((`2012`-`2011`)/`2011`) * 100, 1)) %>%
       mutate(evo2013 = round(((`2013`-`2012`)/`2012`) * 100, 1)) %>%
       mutate(evo2014 = round(((`2014`-`2013`)/`2013`) * 100, 1)) %>%
       mutate(evo2015 = round(((`2015`-`2014`)/`2014`) * 100, 1)) %>%
       mutate(evo2016 = round(((`2016`-`2015`)/`2015`) * 100, 1)) %>%
       mutate(evo2017 = round(((`2017`-`2016`)/`2016`) * 100, 1)) %>%
       mutate(evo2018 = round(((`2018`-`2017`)/`2017`) * 100, 2)) %>%
       mutate(evo2019 = round(((`2019`-`2018`)/`2018`) * 100, 2)) %>%
       mutate(evo = round(((`2019`-`2006`)/`2006`) * 100, 1)) %>%
       #mutate(tcam = round(rowMeans(select(., evo2008:evo2019)), 1)) %>%
       mutate(zone = Zone) %>% 
       select(-`2006`, -`2007`, -`2008`, -`2009`, -`2010`, -`2011`, -`2012`, -`2013`, -`2014`, -`2015`, -`2016`, -`2017`, -`2018`, -`2019`)) 
  
view(donnees_transport_regions %>%
       # filter(sous_secteur=="Transports aériens")) %>%
       group_by(annee, region, sous_secteur) %>%
       summarise(effectif = sum(effectif, na.rm = T)) %>%
       spread(key = region, value = effectif, fill = 0) %>%
       mutate(
         `France métropolitaine` = `Auvergne-Rhône-Alpes` + `Bourgogne-Franche-Comté` +
           `Bretagne` + `Centre-Val de Loire` + `Corse` + `Grand Est` + `Hauts-de-France` +
           `Île-de-France` + `Normandie` + `Nouvelle-Aquitaine` + `Occitanie` + `Pays de la Loire` +
           `Provence-Alpes-Côte d'Azur`
       ) %>%
       gather(key = region, value = effectif, `Auvergne-Rhône-Alpes`:`France métropolitaine`) %>%
       spread(key = sous_secteur, value = effectif, fill = 0) %>%
       mutate(effectif = `Activités de poste et de courrier` + `Autres activités` + `Entreposage, stockage et manutention` + `Services auxiliaires des transports` + `Transports aériens` + `Transports ferroviaires` + `Transports maritimes et fluviaux` + `Transports routiers de marchandises` + `Transports routiers de voyageurs`) %>%
       select(-`Activités de poste et de courrier`, -`Autres activités`, -`Entreposage, stockage et manutention`, -`Services auxiliaires des transports`, -`Transports aériens`, -`Transports ferroviaires`, -`Transports maritimes et fluviaux`, -`Transports routiers de marchandises`, -`Transports routiers de voyageurs`) %>%
       spread(key = annee, value = effectif, fill = 0) %>%
       mutate(evo2004 = round(((`2004`-`2003`)/`2003`) * 100, 1))%>%
       mutate(evo2005 = round(((`2005`-`2004`)/`2004`) * 100, 1)) %>%
       mutate(evo2006 = round(((`2006`-`2005`)/`2005`) * 100, 1)) %>%
       mutate(evo2007 = round(((`2007`-`2006`)/`2006`) * 100, 1)) %>%
       mutate(evo2008 = round(((`2008`-`2007`)/`2007`) * 100, 1)) %>%
       mutate(evo2009 = round(((`2009`-`2008`)/`2008`) * 100, 1)) %>%
       mutate(evo2010 = round(((`2010`-`2009`)/`2009`) * 100, 1)) %>%
       mutate(evo2011 = round(((`2011`-`2010`)/`2010`) * 100, 1)) %>%
       mutate(evo2012 = round(((`2012`-`2011`)/`2011`) * 100, 1)) %>%
       mutate(evo2013 = round(((`2013`-`2012`)/`2012`) * 100, 1)) %>%
       mutate(evo2014 = round(((`2014`-`2013`)/`2013`) * 100, 1)) %>%
       mutate(evo2015 = round(((`2015`-`2014`)/`2014`) * 100, 1)) %>%
       mutate(evo2016 = round(((`2016`-`2015`)/`2015`) * 100, 1)) %>%
       mutate(evo2017 = round(((`2017`-`2016`)/`2016`) * 100, 1)) %>%
       mutate(evo2018 = round(((`2018`-`2017`)/`2017`) * 100, 2)) %>%
       mutate(evo2019 = round(((`2019`-`2018`)/`2018`) * 100, 2)) %>%
       mutate(evo = round(((`2019`-`2003`)/`2003`) * 100, 1)) %>%
       mutate(tcam = round(rowMeans(select(., evo2004:evo2019)), 1)) %>%
       mutate(region_2 = region) %>% 
       select(-`2003`, -`2004`, -`2005`, -`2006`, -`2007`, -`2008`, -`2009`, -`2010`, -`2011`, -`2012`, -`2013`, -`2014`, -`2015`, -`2016`, -`2017`, -`2018`, -`2019`))


view(donnees_regions %>%
  filter(region == "Pays de la Loire") %>%
  group_by(annee, secteur) %>%
  summarise(effectif = sum(effectif, na.rm = T)) %>%
  spread(key = secteur, value = effectif, fill = 0) %>%
  mutate(
    `Ensemble des secteurs marchands non agricoles` = `Activités financières et d'assurance` + `Activités immobilières` + `Activités scientifiques et techniques ; services administratifs et de soutien` + `Administrations publiques, enseignement, santé humaine et action sociale` + `Autres activités de services` + `Cokéfaction et raffinage` + `Commerce et réparation d'automobiles et de motocycles` + `Construction` + `Fabrication d'autres produits industriels` + `Fabrication d'équipements électriques, électroniques et informatiques` + `Fabrication de denrées alimentaires` + `Fabrication de matériels de transport` + `Hébergement et restauration` + `Industries extractives` + `Information et communication` + `Transports et entreposage`) %>%
  gather(key = secteur, value = effectif, `Activités financières et d'assurance`:`Ensemble des secteurs marchands non agricoles`) %>%
  spread(key = annee, value = effectif, fill = 0) %>%
  mutate(secteur = fct_inorder(secteur),
         secteur = fct_relevel(secteur, "Ensemble des secteurs marchands non agricoles", after = Inf)) %>%
  arrange(secteur) %>%
  rename("Secteur économique" = secteur) %>%
  mutate(evo2004 = round(((`2004`-`2003`)/`2003`) * 100, 1)) %>%
  mutate(evo2005 = round(((`2005`-`2004`)/`2004`) * 100, 1)) %>%
  mutate(evo2006 = round(((`2006`-`2005`)/`2005`) * 100, 1)) %>%
  mutate(evo2007 = round(((`2007`-`2006`)/`2006`) * 100, 1)) %>%
  mutate(evo2008 = round(((`2008`-`2007`)/`2007`) * 100, 1)) %>%
  mutate(evo2009 = round(((`2009`-`2008`)/`2008`) * 100, 1)) %>%
  mutate(evo2010 = round(((`2010`-`2009`)/`2009`) * 100, 1)) %>%
  mutate(evo2011 = round(((`2011`-`2010`)/`2010`) * 100, 1)) %>%
  mutate(evo2012 = round(((`2012`-`2011`)/`2011`) * 100, 1)) %>%
  mutate(evo2013 = round(((`2013`-`2012`)/`2012`) * 100, 1)) %>%
  mutate(evo2014 = round(((`2014`-`2013`)/`2013`) * 100, 1)) %>%
  mutate(evo2015 = round(((`2015`-`2014`)/`2014`) * 100, 1)) %>%
  mutate(evo2016 = round(((`2016`-`2015`)/`2015`) * 100, 1)) %>%
  mutate(evo2017 = round(((`2017`-`2016`)/`2016`) * 100, 1)) %>%
  mutate(evo2018 = round(((`2018`-`2017`)/`2017`) * 100, 1)) %>%
  mutate(evo2019 = round(((`2019`-`2018`)/`2018`) * 100, 1)) %>%
  mutate(evo = round(((`2019`-`2003`)/`2003`) * 100, 1)) %>%
  mutate(tcam = round(rowMeans(select(., evo2004:evo2019)), 2)) %>%
  mutate(secteur = `Secteur économique`) %>%
  select(-`2003`, -`2004`, -`2005`, -`2006`, -`2007`, -`2008`, -`2009`, -`2010`, -`2011`, -`2012`, -`2013`, -`2014`, -`2015`, -`2016`, -`2017`, -`2018`, -`2019`))

# Tableau des données brutes d'effectifs par sous-secteur et départements des Pays de la Loire
view(donnees_cogifiees_transports_communes %>%
       filter (TypeZone %in% c("Départements", "Régions")) %>% 
       group_by(Zone, annee, sous_secteur, ape) %>%
       summarise(effectif = sum(effectif, na.rm = T)) %>%
       filter(sous_secteur=="Services auxiliaires des transports") %>%
       # filter(Zone=="Mayenne") %>%
       spread(key = annee, value = effectif, fill = 0) %>%
       mutate(evo2018 = round(((`2018`-`2017`)/`2017`) * 100, 1)) %>%
       mutate(evo2019 = round(((`2019`-`2018`)/`2018`) * 100, 1)))
       

# Tableau des données brutes d'effectifs par sous-secteur en France métro
view(donnees_transport_regions %>%
       #filter(region == "Pays de la Loire") %>%
       group_by(annee, sous_secteur) %>%
       summarise(effectif = sum(effectif, na.rm = T))%>%
       spread(key = annee, value = effectif, fill = 0) %>%
       filter(sous_secteur=="Entreposage, stockage et manutention"))

# Tableau des données brutes d'effectifs par code ape en France métro
view(donnees_transport_regions %>%
       # filter(region == "Pays de la Loire") %>%
       group_by(annee, sous_secteur, ape) %>%
       summarise(effectif = sum(effectif, na.rm = T))%>%
       spread(key = annee, value = effectif, fill = 0) %>%
       filter(sous_secteur=="Transports routiers de voyageurs"))
 
  
# Exemple de cell spec (caractère en blanc dans la colonne evolution pour certaines lignes)
  mutate(evolution = ifelse(
    region %in% c("Pays de la Loire", "France métropolitaine"),
    cell_spec(evolution, color = "white"),
    cell_spec(evolution, color = "black")
  )) %>%
    
    
# Exemple de geom text avec condition   
geom_text(aes(label=ifelse(effectif>3000, paste0((format(effectif, big.mark = " ")), "\nsalariés"), "")), hjust=0.5, vjust=1.5, size=3, color="white", fontface = "bold")   
    
    
    
    
   
  
  