fct_ggplot_soussecteur_pdl_fm <- function(data = donnees_transport_regions,
                                          soussecteur,
                                          breaks_pdl,
                                          breaks_fm,
                                          limits_pdl,
                                          poids_pdl = 20,
                                          titre)
  
{
  
data_in <-  data %>%
    filter(sous_secteur == soussecteur) %>%
    group_by(annee, region) %>%
    summarise(effectif = sum(effectif, na.rm = T)) %>%
    spread(key = region, value = effectif, fill = 0) %>%
    mutate(
      `France métropolitaine` = `Auvergne-Rhône-Alpes` + `Bourgogne-Franche-Comté` +
        `Bretagne` + `Centre-Val de Loire` + `Corse` + `Grand Est` + `Hauts-de-France` +
        `Île-de-France` + `Normandie` + `Nouvelle-Aquitaine` + `Occitanie` + `Pays de la Loire` +
        `Provence-Alpes-Côte d'Azur`
    ) %>%
    gather(key = region, value = effectif, `Auvergne-Rhône-Alpes`:`France métropolitaine`) %>%
    filter(region %in% c("Pays de la Loire", "France métropolitaine")) %>%
    spread(key = region, value = effectif, fill = 0)
    
g <- ggplot(data = data_in)+
      geom_line(mapping = aes(x = annee, y = `Pays de la Loire`/1000), size=1, color="#008ca0")+
      geom_point(mapping = aes(x = annee, y = `Pays de la Loire`/1000), size=1.5, stroke=1.5, pch=21, fill="white", color="#008ca0")+
      geom_line(mapping = aes(x = annee, y = `France métropolitaine`/1000/poids_pdl), size=1, color="#ff6e9b")+
      geom_point(mapping = aes(x = annee, y = `France métropolitaine`/1000/poids_pdl), size=1.5, stroke=1.5, pch=22, fill="white", color="#ff6e9b")+
      scale_x_continuous(breaks = seq(2003, 2018, 5), limits = c(2003, 2018), expand = c(0.05, 0))+
      scale_y_continuous(name = "Pays de la Loire", breaks = breaks_pdl, limits = limits_pdl, expand = c(0.015, 0), sec.axis = sec_axis(~ . * poids_pdl, name = "France métropolitaine", breaks = breaks_fm))+
      theme(plot.title = element_text(size=11),
            plot.caption = element_text(color="grey", size=10, face="plain", hjust=0),
            axis.title.y = element_text(size=10, color = "#008ca0"),
            axis.title.y.right = element_text(size=10, color = "#ff6e9b", angle=90),
            axis.text.y = element_text(size=10, color = "#008ca0"),
            axis.text.y.right = element_text(size=10, color = "#ff6e9b"),
            #axis.line.y = element_blank(),
            axis.title.x = element_blank(),
            axis.text.x = element_text(size = 10, color = "black", hjust = 0.5),
            legend.position = "bottom")+
      labs(title = titre,
           caption = "Champ : section H de la nomenclature NAF rév. 2 (A 21)\nSource : Acoss (données au 31 décembre de chaque année)")
    
    return(g)
}


# fct_ggplot_soussecteur_pdl_fm(soussecteur = "Transports routiers de voyageurs",
#                            breaks_pdl = seq(6, 12, 1),
#                            limits_pdl = c(6,12),
#                            breaks_fm = seq(120, 240, 20),
#                            titre = "Effectifs salariés dans les transports\nroutiers de voyageurs (en milliers)")
# 
# fct_ggplot_soussecteur_pdl_fm(soussecteur = "Transports routiers de marchandises",
#                            breaks_pdl = seq(24, 32, 1),
#                            limits_pdl = c(24, 32),
#                            breaks_fm =  seq(360, 480, 15),
#                            poids_pdl = 15,
#                            titre = "Effectifs salariés dans les transports\nroutiers de marchandises (en milliers)")

#ligne à insérer en haut du rmd, juste après les library
#source("R/fct_ggplot_soussecteur_pdl_fm.R")