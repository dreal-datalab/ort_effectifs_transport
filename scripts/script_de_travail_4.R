library(bsplus)
library(COGiter)
library(dplyr)
library(DT)
library(formattable)
library(ggplot2)
library(highcharter)
library(htmltools)
library(htmlwidgets)
library(jpeg)
library(kableExtra)
library(leaflet)
library(leaflet.extras)
library(lubridate)
#library(phantomjs)
#library(plotly)
library(png)
library(rAmCharts)
library(sf)
library(shinyWidgets)
library(shiny)
library(shinyBS)
library(shinycustomloader)
library(shinydashboard)
library(shinydreal)
library(tidyverse)
library(viridis)
library(webshot)

load("app/ort_effectifs_transports.RData")
load("app/fonds_de_carte_52.RData")

test<-donnees_cogifiees_transports_communes %>%
  filter(TypeZone=="Epci") %>%
  #filter(annee==input$mon_annee) %>%
  filter(annee==2017) %>%
  group_by(Zone, CodeZone, annee) %>%
  summarise(effectif = sum(effectif, na.rm = T)) %>%
  ungroup() %>% 
  rename(EPCI = CodeZone)

test_2 <- epci_geo_52 %>% 
  right_join(test) %>% 
  select(-NOM_EPCI, -NATURE_EPCI)

bins <- quantile(test$effectif, probs = c(0, 0.1, 0.4, 0.7, 0.9, 1), na.rm=TRUE)
pal <- colorBin("YlOrRd", domain = test_2$effectif, bins = bins)

leaflet(test_2) %>%
  # addProviderTiles(providers$CartoDB.Positron) %>%
  # addTiles(attribution = HTML(paste(span(style="color:#00465a","Source : Acoss (données au 31 décembre")))) %>%
  # setView(lng = -1.5533600, lat = 47.2172500, zoom = 8) %>%
  addControl(html = tags$div(
    HTML(paste("<b>",
               "Effectifs salariés dans le secteur<br>des transports et de l'entreposage<br>en",
               annee,
               "</b>")),
    align="center"),
    position = "topright") %>%
  addControl(html = tags$div(
    HTML(paste("Source : Acoss (données au 31 décembre)"))), position = "bottomleft") %>%
  # fitBounds(base::min(cadrage_carte_departement()$lon),base::min(cadrage_carte_departement()$lat),
  #           base::max(cadrage_carte_departement()$lon),base::max(cadrage_carte_departement()$lat)) %>%
  # addPolygons(data = test, weight = 1.2,
  #             color = "grey",
  #             fillOpacity = 0) %>%
  addPolygons(data = test_2, fillColor = ~pal(effectif), weight = 1, smoothFactor = 0.1,
              opacity = 1, fillOpacity = 1, color = "white",
              dashArray = "1",
              labelOptions = labelOptions(
                style = list("font-weight" = "normal", padding = "3px 8px"),
                textsize = "15px",
                direction = "auto")) %>%
  addPolygons(data = departements_geo_52, weight = 2, 
              opacity = 1, fillOpacity = 0, color = "black") %>%
  addLegend(pal = pal, values = ~effectif, opacity = 1, title = "Nombre de salariés",
            position = "bottomright",
            na.label = "Aucun salarié") %>%
  addFullscreenControl()


